// modified from [ http://www.learningjquery.com/2007/10/improved-animated-scrolling-script-for-same-page-links/ ]
// changed "$" to "jQuery"
// included removeClass/addClass (ln25-26, ln30)

jQuery(document).ready(function() {

	function filterPath(string) {
		return string
		.replace(/^\//,'')
		.replace(/(index|default).[a-zA-Z]{3,4}$/,'')
		.replace(/\/$/,'');
	}

	var locationPath = filterPath(location.pathname);
	jQuery('a[href*=#]').each(function() {
		var thisPath = filterPath(this.pathname) || locationPath;

		if ( locationPath == thisPath && (location.hostname == this.hostname || !this.hostname) && this.hash.replace(/#/,'') ) {
			var $target = jQuery(this.hash), target = this.hash;

			if ( target ) {
				var targetOffset = $target.offset().top;
				jQuery(this).click(function(event) {
					event.preventDefault();
					jQuery('.elucidate').removeClass('target');
					$target.addClass('elucidate');
					jQuery('html, body').animate({scrollTop: targetOffset}, 400, function() {
						location.hash = target;
					});
					$target.addClass('target');
				});
			}

		}

	});

});