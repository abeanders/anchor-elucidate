<?php


/**
 * Plugin Name: Anchor Elucidate
 * Description: A plugin that makes it apparent what anchor you have reached on the page. Don't just jump, elucidate!
 * Version: 0.0
 * Author: Abe Millett
 * License: GPL3
 */


if ( ! is_admin() )
	add_action( 'wp_enqueue_scripts', 'elucidate_enqueue' );
function elucidate_enqueue() {

	wp_enqueue_style( 'elucidate_style', plugins_url( 'anchor-elucidate.css', __FILE__ ) );
	wp_enqueue_script( 'elucidate_script', plugins_url( 'anchor-elucidate.js', __FILE__ ), array('jquery') );

}


?>