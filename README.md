#Anchor Elucidate

A WordPress plugin that makes it apparent what anchor you have reached on the page. Don't just jump, elucidate!